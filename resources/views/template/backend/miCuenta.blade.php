@extends('layouts.layout-backend')

@section('title')
   Mi Cuenta - El Portal Del Litoral Central
@endsection

@section('css')
	<link href="{{ asset('css/jquery.fancybox.min.css') }}" rel="stylesheet">
@endsection
@section('content')
  <!-- Page Content -->  
    {{--  @include('template.partial.search-full') --}}
    <br><br>
    <!-- Page Content -->
    <div class="container" style="padding: 20px">
		<!-- Portfolio Item Heading -->
		<h1 class="my-4" style="color:#37e5df !important;">Bienvenido {{ Auth::user()->name }}</h1>        

        <div style="border-radius: 25px;    background-color: rgba(4, 4, 56, 0.4);    padding: 20px;    width: 100%;    min-height: 350px; ">

            <!-- Page Content -->
                <div class="container">

                  <h2 class="text-center text-lg-left"><i class="fas fa-tachometer-alt"></i> Panel de Control</h2>
                  <br>
                  <br>

                  <div class="row text-center text-lg-left text-center">

                    <div class="col-lg-3 col-md-4 col-xs-6 text-center" style="padding-bottom: 15px;">
                      <a href="#" class="d-block mb-4 h-100">
                        <i class="fas fa-file-alt fa-15em" style="padding-bottom: 10px;"></i> <br>
                        <h5>Mis Publicaciones</h5>
                      </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-xs-6 text-center" style="padding-bottom: 15px;">
                      <a href="#" class="d-block mb-4 h-100">
                        <i class="fas fa-comments fa-15em" style="padding-bottom: 10px;"></i> <br>
                        <h5>Mis Mensajes</h5>
                      </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-xs-6 text-center" style="padding-bottom: 15px;">
                      <a href="#" class="d-block mb-4 h-100">
                        <i class="fas fa-address-card fa-15em" style="padding-bottom: 10px;"></i> <br>
                        <h5>Mis Datos</h5>
                      </a>
                    </div>                 
                    <div class="col-lg-3 col-md-4 col-xs-6 text-center" style="padding-bottom: 15px;">
                      <a href="#" class="d-block mb-4 h-100">
                        <i class="fas fa-money-check-alt fa-15em" style="padding-bottom: 10px;"></i> <br>
                        <h5>Mis Plan</h5>
                      </a>
                    </div>                   

                  
                  </div>
                </div>
            <!-- /.container -->
        </div>


        


    </div>


@endsection

@section('js')
	<script src="{{ asset('js/jquery.fancybox.min.js') }}"></script>
@endsection



     




