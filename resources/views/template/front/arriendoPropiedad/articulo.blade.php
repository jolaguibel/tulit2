@extends('layouts.layout')

@section('title')
    El Portal Del Litoral Central
@endsection

@section('css')
	<link href="{{ asset('css/jquery.fancybox.min.css') }}" rel="stylesheet">
@endsection


@section('content')

  <!-- Page Content -->  
    @include('template.partial.search-full')

    <!-- Page Content -->
    <div class="container">

		<!-- Portfolio Item Heading -->
		<h1 class="my-4">Cabaña familiar
		<small>El Quisco</small>
		</h1>


	     <!-- Portfolio Item Row -->
	     <div class="row descripcion-propiedad">

	        <div class="col-md-6" style="padding-bottom: 25px;">
	        	<div id="demo" class="carousel slide" data-ride="carousel">
				  <ul class="carousel-indicators">
				    <li data-target="#demo" data-slide-to="0" class="active"></li>
				    <li data-target="#demo" data-slide-to="1"></li>
				    <li data-target="#demo" data-slide-to="2"></li>
				  </ul>
				  <div class="carousel-inner">
				    <div class="carousel-item active">
				    	<a data-fancybox="gallery" href="https://http2.mlstatic.com/S_16051-MLC20113043599_062014-F.jpg"><img src="https://http2.mlstatic.com/S_16051-MLC20113043599_062014-F.jpg" class="carrusel-propiedad"></a>
						<div class="carousel-caption">
							<h3>Los Angeles</h3>
							<p>We had such a great time in LA!</p>
						</div>   
				    </div>
				    <div class="carousel-item">
				      	<a data-fancybox="gallery" href="https://http2.mlstatic.com/none-D_NQ_NP_16092-MLC20113042286_062014-F.jpg"><img src="https://http2.mlstatic.com/none-D_NQ_NP_16092-MLC20113042286_062014-F.jpg" class="carrusel-propiedad"></a>
						<div class="carousel-caption">
							<h3>Chicago</h3>
							<p>Thank you, Chicago!</p>
						</div>   
				    </div>
				    <div class="carousel-item">		

						<a data-fancybox="gallery" href="https://http2.mlstatic.com/none-D_NQ_NP_16097-MLC20113043630_062014-F.jpg"><img src="https://http2.mlstatic.com/none-D_NQ_NP_16097-MLC20113043630_062014-F.jpg" class="carrusel-propiedad"></a>
						<div class="carousel-caption">
							<h3>New York</h3>
							<p>We love the Big Apple!</p>
						</div>   
				    </div>
				  </div>
				  <a class="carousel-control-prev" href="#demo" data-slide="prev">
				    <span class="carousel-control-prev-icon"></span>
				  </a>
				  <a class="carousel-control-next" href="#demo" data-slide="next">
				    <span class="carousel-control-next-icon"></span>
				  </a>
				</div>
	        </div>

	        <div class="col-md-6">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active tab" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link tab" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
					</li>
					<li class="nav-item">
						<a class="nav-link tab" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">								
						<h4 class="my-3">Project Description</h4>
						<p class="description-propiedad">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>
						<h3 class="my-3">Project Details</h3>
						<ul class="description-propiedad">
						<li>Lorem Ipsum</li>
						<li>Dolor Sit Amet</li>
						<li>Consectetur</li>
						<li>Adipiscing Elit</li>
						</ul>
					</div>
					<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
					<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
				</div>



	        </div>

	    </div>
	      <!-- /.row -->
	      <!-- /.map -->

	    <h4 class="my-4">Ubicación en el mapa</h4>
		<div class="row">
		<!--The div element for the map -->
		<div id="map"></div>
		<script>
			// Initialize and add the map
			function initMap() {
			  // The location of Uluru
			  var uluru = {lat: -33.3942691, lng: -71.7217562};
			  // The map, centered at Uluru
			  var map = new google.maps.Map(
			      document.getElementById('map'), {zoom: 10, center: uluru});
			  // The marker, positioned at Uluru
			  var marker = new google.maps.Marker({position: uluru, map: map});
			}
			</script>
			<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnxZQw92wExvw6VWPC4nww2psARwSuy5g&callback=initMap">
		</script>
		</div>

	     <!-- /.map -->
	
		<hr class="style12">
		<!-- Related Projects Row -->
		<h3 class="my-4">Publicaciones similares</h3>

		<div class="row" style="padding-bottom: 20px; padding-top: 20px;">
		  <div class="col-md-3 col-sm-6 col-xs-12 calugas-resultado">
		    <a href="/propiedad">
		      <img src="http://www.traslasierra.com/media/688d7493-f2ca-4d64-88e7-f6e0d6f42408/sTxeBQ/Alojamiento/Cabanias%20y%20complejos/El%20Eden%20-%20Los%20Hornillos/02%20cabana.jpg" alt="Snow" class="img-fluid">        
		      <div class="text-calugas-resultado">numero 1</div>
		    </a>
		  </div>
		  <div class="col-md-3 col-sm-6 col-xs-12 calugas-resultado">
		    <a href="/propiedad">
		      <img src="http://www.turismoencanto.com/wp-content/uploads/2013/12/IMG_6679.jpg" alt="Snow" class="img-fluid">       
		      <div class="text-calugas-resultado">numero 1</div>
		    </a>
		  </div>
		  <div class="col-md-3 col-sm-6 col-xs-12 calugas-resultado">
		    <a href="/propiedad">
		      <img src="http://www.rucaaliwen.cl/images/cabana2.jpg" alt="Snow" class="img-fluid">         
		      <div class="text-calugas-resultado">numero 1</div>
		    </a>
		  </div>
		  <div class="col-md-3 col-sm-6 col-xs-12 calugas-resultado">
		    <a href="/propiedad">
		      <img src="http://www.cabsanmiguel.com/es/wp-content/uploads/2013/10/merlo-san-luis-caba%C3%B1as.jpg" alt="Snow" class="img-fluid">       
		      <div class="text-calugas-resultado">numero 1</div>
		    </a>
		  </div>
		</div>

	      <!-- /.row -->	

    </div>
    <!-- /.container -->


@endsection

@section('js')
	<script src="{{ asset('js/jquery.fancybox.min.js') }}"></script>



@endsection



     




