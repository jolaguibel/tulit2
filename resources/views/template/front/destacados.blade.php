<div class="destacados-full">
<!-- Page Content -->
    <div class="container">
      <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-md-6">
          <div id="demo" class="carousel slide" data-ride="carousel">
            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <a href="#"><img src="https://http2.mlstatic.com/S_16051-MLC20113043599_062014-F.jpg" class="carrusel-propiedad"></a>
              <div class="carousel-caption">
                <h3>Los Angeles</h3>
                <p>We had such a great time in LA!</p>
              </div>   
              </div>
              <div class="carousel-item">
                  <a href="#"><img src="https://http2.mlstatic.com/none-D_NQ_NP_16092-MLC20113042286_062014-F.jpg" class="carrusel-propiedad"></a>
              <div class="carousel-caption">
                <h3>Chicago</h3>
                <p>Thank you, Chicago!</p>
              </div>   
              </div>
              <div class="carousel-item">   
                <a href="#"><img src="https://http2.mlstatic.com/none-D_NQ_NP_16097-MLC20113043630_062014-F.jpg" class="carrusel-propiedad"></a>
              <div class="carousel-caption">
                <h3>New York</h3>
                <p>We love the Big Apple!</p>
              </div>   
              </div>
            </div>
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
              <span class="carousel-control-prev-icon" style="color: red !important;"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
              <span class="carousel-control-next-icon"></span>
            </a>
          </div>
        </div>

          <div class="col-md-6">
            <div class="col calugas-destacados">              

                <img src="http://img.soy-chile.cl/Fotos/2014/02/14/file_20140214160535.jpg" alt="Snow" >        
                <div class="bottom-right">Tour por la bahia en lancha</div>

            </div>
            <div class="col calugas-destacados">
                
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiy3nMINX0uX2dPg-Z1q5eUBpIV9ktO1g9ESTXTQ3h5raP06T9Og" alt="Snow" >        
                <div class="bottom-right">Cabalgatas Familiares</div>

            </div>
          </div>

      </div>.

        <!-- /.row -->
    </div>
    <!-- /.container -->

</div>