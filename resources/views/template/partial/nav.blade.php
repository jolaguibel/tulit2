<!--
<nav hidden>
  <div class="nav-header">
    <a href="{{ URL::to('/') }}">
      <div class="brand logo">TuLitoral <div class="nube-logo"><i class="fas fa-cloud"></i></div></div>
    </a>
    <button class="toggle-bar"><span class="fa fa-bars"></span></button>  
  </div>                
  <ul class="menu">
    <li><a href="#" onclick="openNav()"><i class="fas fa-search"></i>  Buscar Arriendos <span class="sr-only">(current)</span></a></li>
    <li class="dropdown">
        <a href="#"><span class="fas fa-chess-knight"></span></a>
        <ul class="dropdown-menu">
            <li><a href="#">Menu 1</a></li>                                    
            <li><a href="#">Menu 2</a></li>                                    
            <li><a href="#">Menu 3</a></li>                                    
            <li><a href="#">Menu 4</a></li>   
        </ul>  
    </li>
    <li><a href="#">About</a></li>
    <li><a href="#">Service</a></li>
  </ul>
</nav>

-->


<nav hidden>
    <div class="nav-header">
        <a href="{{ URL::to('/') }}">
           <div class="brand logo">TuLitoral</div>
        </a>
        <button class="toggle-bar">
            <span class="fa fa-bars"></span>
        </button> 
    </div>                
    <ul class="menu">
        <li><a href="#" onclick="openNav()"><i class="fas fa-search"></i>  Buscar Arriendos <span class="sr-only">(current)</span></a></li>  
        <li>&nbsp;</li>
        <li class="megamenu" data-width="350" >
            <a href="#"> <i class="fas fa-utensils"></i> ¿Donde comer?</a>
            <div class="megamenu-content">
                <div class="sample-text">
                    <ul>
                        <li>
                        <a href="#"><i class="fas fa-utensils"></i> Restaurant <span class="sr-only"></span></a>
                        </li>
                        <li>
                        <a href="#"><i class="fas fa-fish"></i> Caletas <span class="sr-only"></span></a>
                        </li>
                        <li>
                        <a href="#"><i class="fas fa-shipping-fast"></i>  Picadas al paso <span class="sr-only"></span></a>
                        </li>
                        <li>
                        <a href="#"><i class="fas fa-cookie-bite"></i>  Pan amasado / Tortillas <span class="sr-only"></span></a>
                        </li>
                    </ul>                  
                </div>
            </div>
        </li>
        <li class="megamenu" data-width="350" >
            <a href="#"> <i class="fas fa-globe-americas"></i> Turismo</a>
            <div class="megamenu-content">
                <div class="sample-text">
                    <ul>
                        <li>
                          <a href="#"><i class="fas fa-binoculars"></i> Agencias Turismo <span class="sr-only"></span></a>
                        </li>
                        <li>
                          <a href="#"><i class="fas fa-car"></i>  Renta Car <span class="sr-only"></span></a>
                        </li>
                        <li>
                          <a href="#"><i class="fas fa-camera"></i> Atractivos Turísticos <span class="sr-only"></span></a>
                        </li>
                        <li>
                          <a href="#"><i class="fas fa-theater-masks"></i> Programas Culturales <span class="sr-only"></span></a>
                        </li>
                        <li>
                          <a href="#"><i class="fas fa-ship"></i> Paseo Lancha / Caballo  <span class="sr-only"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="megamenu" data-width="350" >
            <a href="#"> <i class="fas fa-briefcase"></i> Datos Utiles</a>
            <div class="megamenu-content">
                <div class="sample-text">
                    <ul>
                        <li>
                        <a href="#"><i class="fas fa-building"></i> Municipalidades <span class="sr-only"></span></a>
                        </li>
                        <li>
                        <a href="#"><i class="fas fa-tint"></i>  Servicios Básico <span class="sr-only"></span></a>
                        </li>
                        <li>
                        <a href="#"><i class="fas fa-map-marked"></i>  Comisarias <span class="sr-only"></span></a>
                        </li>
                        <li>
                        <a href="#"><i class="fas fa-fire-extinguisher"></i>  Bomberos <span class="sr-only"></span></a>
                        </li>                     
                        <li>
                        <a href="#"><i class="fas fa-ambulance"></i>  Ambulancias <span class="sr-only"></span></a>
                        </li>
                        <li>
                        <a href="#"><i class="fas fa-hospital"></i>  Atención de Salud <span class="sr-only"></span></a>
                        </li>
                        <li>
                        <a href="#"><i class="fas fa-church"></i>  Iglesias <span class="sr-only"></span></a>
                        </li> 
                        <li>
                        <a href="#"><i class="fas fa-graduation-cap"></i>  Colegios <span class="sr-only"></span></a>
                        </li>  
                    </ul>                  
                </div>
            </div>
        </li>
        @if (Route::has('login'))

             @guest
                <li class="nav-item">
                    <a href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i>Ingresar</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('register') }}"><i class="fas fa-user"></i>Registrate</a>
                </li>
            @else
                <li class="megamenu" data-width="260">
                    <a href="#"> <i class="fas fa-user"></i> {{ Auth::user()->name }} </a>
                    <div class="megamenu-content">
                        <div class="sample-text">
                            <ul>
                                <li>
                                <a href="{{ url('/micuenta') }}"><i class="fas fa-user-circle"></i>  Mi Cuenta <span class="sr-only"></span></a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt"></i> Salir
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>                   
                            </ul>
                        </div>
                    </div>
                </li>
            @endguest     
        @endif    

    </ul>
</nav>
