<div class="search-full">
  <div class="container"> 
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <button class="btn btn-info" type="button"> <i class="fas fa-search"></i> Búsqueda Rápida </button>
      </div>
      <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1">            
    </div>
    <div class="txt_busqueda_avanzada" onclick="openNav()" >
      <i class="fas fa-search"></i> Filtro avanzado búsqueda
    </div> 
  </div>
</div>