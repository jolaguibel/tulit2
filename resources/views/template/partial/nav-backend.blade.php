<nav hidden>
    <div class="nav-header">
        <a href="{{ URL::to('/') }}">
           <div class="brand logo">TuLitoral</div>
        </a>
        <button class="toggle-bar">
            <span class="fa fa-bars"></span>
        </button> 
    </div>                
    <ul class="menu">        
        @if (Route::has('login'))
             @guest
                <li class="nav-item">
                    <a href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i>Ingresar</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('register') }}"><i class="fas fa-user"></i>Registrate</a>
                </li>
            @else
                <li> 
                    <a href="{{ url('/') }}"><i class="fas fa-file-alt"></i> Mis Publicaciones</a>
                </li> 
                <li> 
                    <a href="{{ url('/') }}"><i class="fas fa-comments"></i> Mis Mensajes</a>
                </li> 
                <li> 
                    <a href="{{ url('/') }}"><i class="fas fa-address-card"></i> Mis Datos</a>
                </li> 
                <li>             
                    <a href="{{ url('/') }}"><i class="fas fa-money-check-alt"></i> Mi  Plan</a>
                </li> 
                <li class="megamenu" data-width="260">
                    <a href="#"> <i class="fas fa-user"></i> {{ Auth::user()->name }} </a>
                    <div class="megamenu-content">
                        <div class="sample-text">
                            <ul>
                                <li>
                                <a href="{{ url('/micuenta') }}"><i class="fas fa-user-circle"></i>  Mi Cuenta <span class="sr-only"></span></a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt"></i> Salir
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>                   
                            </ul>
                        </div>
                    </div>
                </li>
            @endguest     
        @endif      
    </ul>
</nav>
