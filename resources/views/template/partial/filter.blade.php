<div id="mySidenav" class="sidenav">
  <br>
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <br>
  <div class="filtro_avanzado">
  	<div class="container">
  		
  		<br>
  		<h4><i class="fas fa-filter"></i> Busqueda Avanzada </h4>
  		<br>
  		<div class="form-group">
		    <label for="exampleSelect1">Seleciona Localidad</label>
		    <select class="form-control" id="exampleSelect1">
				<option>Algarrobo</option>
				<option>San Antonio</option>
				<option>Isla Negra</option>
				<option>El quisco</option>
				<option>El Tabo</option>
				<option>Cartagena</option>
				<option>Santo Domingo</option>
				<option>Las Cruces</option>
				<option>Punta de Tralca</option>
				<option>Mirasol</option>
				<option>Llolleo</option>
				<option>El Totoral</option>
				<option>Tunquen</option>
				<option>San Sebastian</option>
				<option>El yeco</option>
				<option>Lo Abarca</option>
				<option>El Convento</option>
				<option>Rocas de Santo Domingo</option>
				<option>Concumen</option>
				<option>El Tabito</option>
				<option>Costa Azul</option>
				<option>Leyda</option>
		    </select>
	    </div>
	    <div class="form-group">
		    <label for="exampleSelect1">Tipo Propiedad</label>
		    <select class="form-control" id="exampleSelect1">
		    	<option>Hotel / Hostal</option>
				<option>Casa</option>
				<option>Departamento</option>
				<option>Pieza</option>
				<option>Terreno</option>
		    </select>
	    </div>
		<div class="form-check">
			<label class="form-check-label">
				<input type="checkbox" class="form-check-input">
				Baño Privado
			</label>	
		</div>
		<div class="form-check">	
			<label class="form-check-label">
				<input type="checkbox" class="form-check-input">
				Desayuno Incluido
			</label>
		</div>
		<div class="form-check">	
			<label class="form-check-label">
				<input type="checkbox" class="form-check-input">
				Anfitrion Para Estadia
			</label>
		</div>

		<br><br>
		<button type="submit" class="btn btn-primary">Buscar </button>


  	</div>
  </div>
</div>


