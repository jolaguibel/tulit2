<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>


        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TuLitoral.cl  &middot;  @yield('title')</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Bootstrap CSS -->
  
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/nav.css') }}" rel="stylesheet">
        <link href="{{ asset('css/filter/filter.css') }}" rel="stylesheet">
        @yield('css')   

       
        <!-- Scripts -->
        <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

        <!--Local Script-->
        <script src="{{ asset('js/app.js') }}" ></script>
        <script src="{{ asset('js/menu.js') }}" ></script>
        <script src="{{ asset('js/coreNavigation.js') }}"></script>
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}" ></script>
        <script src="{{ asset('js/filter/filter.js') }}" ></script>
              

        @yield('js')


        <link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Kirang+Haerang" rel="stylesheet"> 

        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Hammersmith+One" rel="stylesheet"> 

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

         
        <meta property='og:image' content='http://yoursite.com/INSERT_YOUR_IMAGE.jpg'/>

        <meta property="og:url"                content="http://www.nytimes.com/2015/02/19/arts/international/when-great-minds-dont-think-alike.html" />
        <meta property="og:type"               content="article" />
        <meta property="og:title"              content="When Great Minds Don’t Think Alike" />
        <meta property="og:description"        content="How much does culture influence creative thinking?" />
        <meta property="og:image"              content="http://static01.nyt.com/images/2015/02/19/arts/international/19iht-btnumbers19A/19iht-btnumbers19A-facebookJumbo-v2.jpg" />


    
    </head>
    <body>
        @include('template.partial.nav-backend')
        @include('template.partial.filter')
        <div class="transbox" style="min-height: 100vh;">
            @yield('content')
        </div>

    </body>
</html>
