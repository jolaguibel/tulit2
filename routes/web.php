<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front');
});


//loggin para revisar
Auth::routes();


#--------------------------------------------------------------------------------------

####    FRONT END   ###
	#DASHBOARD
	Route::get('/home', 'HomeController@index')->name('home');


	#PROPIEDADES
	Route::get('/propiedad','Front\ArriendoPropiedadController@getArticulo');



#--------------------------------------------------------------------------------------
###     BACK END   ###
	#CUENTA USUARIO
	Route::group(['middleware' => ['auth']], function () {
		Route::get('/micuenta','backend\PerfilUsuarios@miCuenta');
	});


#--------------------------------------------------------------------------------------