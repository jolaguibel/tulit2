 $( document ).ready(function() {
 	/*
	$('nav').coreNavigation({
	    menuPosition: "right", // left, right, center, bottom
	    container: false, // true or false
	    mode: 'fixed',
        dropdownEvent: 'click',
	    onInit: function(){
	      console.log('Init coreNav');
	    }
	});
	*/

	$('nav').coreNavigation({
	    menuPosition: "right", // left, right, center, bottom
	    container: false, // true or false
	    animated: true,
	    mode: 'fixed',
	    animatedIn: 'flipInX',
	    animatedOut: 'bounceOut',
	    dropdownEvent: 'click', // Hover, Click & Accordion
	    onOpenDropdown: function(){
	        console.log('open');
	    },
	    onCloseDropdown: function(){
	        console.log('close');
	    },
	    onOpenMegaMenu: function(){
	        console.log('Open Megamenu');
	    },
	    onCloseMegaMenu: function(){
	        console.log('Close Megamenu');
	    }
	});
});